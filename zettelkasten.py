# -*- coding: utf-8 -*-
"""
Created on Sun Dec 20 11:19:30 2020

@author: Thomas
"""

import os
from datetime import datetime, timedelta

from forms import RegistrationForm, LoginForm
from flask import Flask, render_template, request, session, redirect, url_for, flash
from pymongo import MongoClient
from dotenv import load_dotenv
from bson.objectid import ObjectId

load_dotenv()

def create_app():
    app = Flask(__name__)
    
    app.secret_key= os.environ.get("SESSION_KEY")
    app.permanent_session_lifetime = timedelta(minutes=60)
    client = MongoClient(os.environ.get("MONGODB_URI"))
    app.db = client.zettelkasten


    @app.route("/login", methods=["GET", "POST"])
    def login():
      login_form = LoginForm()
      if login_form.validate_on_submit():
         entry_username = request.form.get("username")
         entry_password = request.form.get("password")
         user = app.db.user.find_one({'username': entry_username, 'password': entry_password})
         if (user == None):
            flash('There is no User that matches your credentials!')
            return redirect(url_for("login"))
         else:
            session['user'] = entry_username
            return redirect(url_for("home"))            
         ## check login
      else:
         flash(login_form.errors, "error")
         print(login_form.errors)
         return render_template("login.html", title="Login Form", form=login_form)
      
      
    @app.route("/logout")
    def logout():
        session.pop("user", None);
        flash("You have been logged out!", "info")
        return redirect(url_for("login"))
    
    @app.route("/register", methods=["GET", "POST"])
    def register():
        registration_form = RegistrationForm()
        if registration_form.validate_on_submit():
           entry_username = request.form.get("username")
           entry_email    = request.form.get("email")
           entry_password = request.form.get("password")
           entry_date     = datetime.now()
           
           app.db.user.insert({"username": entry_username, 
                               "email": entry_email, 
                               "password": entry_password,
                               "date": entry_date})       
           flash(f"Account created for {registration_form.username.data}!", 'success')
           return redirect(url_for("home"))
        else:   
           print(registration_form.errors)      
        return render_template('register.html', title="Registration Form", form=registration_form)
        
    @app.route("/delete/<_id>")
    def delete(_id=None):
        app.db.zettel.delete_one({ "_id" : ObjectId(_id)})
        return home()
        
    @app.route("/", methods=["GET", "POST"])
    def home():
        if "user" in session:
           user = session["user"]
        else:
           return redirect(url_for("login"))
           
        if request.method == "POST":
            entry_content = request.form.get("content")
            entry_title = request.form.get("title")
            entry_date = datetime.now()

            app.db.zettel.insert({"title": entry_title, 
                                  "content": entry_content, 
                                  "date": entry_date,
                                  "user": user})        
  
        entry_display = [(e['title'], 
                         e['content'], 
                         e['date'].strftime("%b %d") ,
                         e['_id'])
                         for e in  app.db.zettel.find({}).sort('date', -1) 
                         ]
       
        return render_template("home.html", entries=entry_display, user=user)
        
        
    return app 
    
    
#if __name__ == "__main__":
#  app.run(port=4999)    